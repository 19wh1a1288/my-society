 

import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

 

@Component({

  selector: 'app-lservice',

  templateUrl: './lservice.component.html',

  styleUrls: ['./lservice.component.css']

})

export class LserviceComponent implements OnInit {

 

  constructor(private router: Router) { }

 

  ngOnInit(): void {

  }

  gotohelpdesk()

  {

    this.router.navigate(['helpdesk']);

  }

  gotoamenities()

  {

    this.router.navigate(['amenities']);

  }

  gotoemergency()

  {

    this.router.navigate(['emergency']);

  }

  gotoowners()

  {

    this.router.navigate(['owners']);

  }
  gotodaily()

  {

    this.router.navigate(['dailyhelp']);

  }
  gotonotice()

  {

    this.router.navigate(['noticeboard']);

  }
  gotocovid(){
    this.router.navigate(['covid'])
  }

 

 

}


