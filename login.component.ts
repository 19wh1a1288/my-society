



import { Component, OnInit } from '@angular/core';
import { FormControl ,FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import{ VendorService} from '../vendor.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    title="Login";

    constructor(private router: Router,private vendor:VendorService) { }

    ngOnInit(): void {
    }

    email!:string;
    password!:string;
    contactFrom = new FormGroup({

      email: new FormControl('', [Validators.required,Validators.email]),
      password: new FormControl('', [Validators.required])
        })



   onSubmit(){
      console.log(this.contactFrom.value);
      this.vendor.login(this.email,this.password).subscribe(data=>
        {
          if(data!=null)
          this.router.navigate(['login']);
          else
          alert("Enter correct email id and password");
        });
    }


  }


/* import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor() { }

    ngOnInit(): void {
    }

  title = 'Registration Form';
}*/