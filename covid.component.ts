import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { FormBuilder,FormGroup } from '@angular/forms';

import { CovidData } from '../models/coviddata';

import { CovidsService } from '../covids.service';

 

@Component({

  selector: 'app-owners',

  templateUrl: './covid.component.html',

  styleUrls: ['./covid.component.css']

})

export class CovidComponent implements OnInit {

 

  constructor(private covidservice: CovidsService, private formBuilder: FormBuilder) { }

  covids: CovidData[];

  covidForm: FormGroup;

  modalMod: string;

  //openedPatient: PatientData;

 

  ngOnInit(): void {

    this.covids = this.covidservice.GetCovidDatas();

    this.covidForm = this.formBuilder.group({

      Name: '',
      Houseno:'',

      //lastName: '',

      //insurance: '',

      //phoneNumber: '',
      Members:'',
      Mvaccinated:'',

      //address: ''

    });

  }

  AddCovidButton(): void {

    this.modalMod = 'add';

    this.ClearCovidForm();

  }

  AddUpdateCovid() {

    switch (this.modalMod) {

      case 'add':

        this.covidForm.value.id = this.covidservice.GetCovidDatas().length + 1;

        this.covidservice.AddCovid(this.covidForm.value);

        this.ClearCovidForm();

        break;

      case 'update':

        const covidControls = this.covidForm.controls;

        for (const key in covidControls) {

          if (covidControls.hasOwnProperty(key)) {

            const control = covidControls[key];

            //this.openedPatient[key] = control.value;

          }

        }

        break;

    }

  }

  RemoveCovid(covids: CovidData) {

    const index = this.covids.indexOf(covids);

    this.covids.splice(index, 1);

  }

  ClearCovidForm() {

    //this.ControlSetValueLoop(undefined);

  }

  EditOpenModal(pati: CovidData) {

    this.modalMod = 'update';

    //this.openedPatient = pati;

    this.ControlSetValueLoop(pati);

  }

  ControlSetValueLoop(pati: CovidData) {

    const covidControls = this.covidForm.controls;

    for (const key in covidControls) {

      if (covidControls.hasOwnProperty(key)) {

        const control = covidControls[key];

        if (pati === undefined) {

          control.setValue('');

        } else {

          //control.setValue(pati[key]);

        }

      }

    }

  }

 

}
