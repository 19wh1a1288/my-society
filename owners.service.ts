import { Injectable } from '@angular/core';

import { OwnerData } from './models/ownerdata';

 

@Injectable({

  providedIn: 'root'

})

export class OwnersService {

 

  constructor() { }

  ownerData: OwnerData[] = this.GetExampleJSON().ownerRecords;

 

  GetExampleJSON() {

    const exampleJSON = {

      ownerRecords: [

        //{ Name: 'Ambulance',phoneNumber: '108' },

        //{ firstName: 'Ambulance', phoneNumber: '102' },

        //{ firstName: 'Police', phoneNumber: '100' },

        //{ firstName: 'Fire Station', phoneNumber: '101'}

      ]

    };

    return exampleJSON;

  }

 

  GetPatientDatas() {

    return this.ownerData;

  }

  AddPatient(newPatient: OwnerData) {

    this.ownerData.push(newPatient);

  }

}





 