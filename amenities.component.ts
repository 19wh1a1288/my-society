import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

 

@Component({

  selector: 'app-amenities',

  templateUrl: './amenities.component.html',

  styleUrls: ['./amenities.component.css']

})

export class AmenitiesComponent implements OnInit {

 

  constructor(private router: Router) { }

 

  ngOnInit(): void {

  }

  gotofunctionhall()

  {

    this.router.navigate(['functionhall']);

  }

  gotoextrastore()

  {

    this.router.navigate(['extrastore']);

  }

 

}

 